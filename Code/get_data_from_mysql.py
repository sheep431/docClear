import pymysql
import pandas as pd

def get_data_from_mysql(mysql,user="sheep431",password='3Apples!!',db='x5sys',port=3306):
    curhost = "rm-bp199cccyndb54urf4o.mysql.rds.aliyuncs.com"

    conn = pymysql.connect(
        host=curhost,
        port=port,
        user=user,
        password=password,
        db=db,
        charset="utf8"
    )

    cursor = conn.cursor()
    sql = mysql
    res=cursor.execute(sql)
    df = pd.read_sql(sql=sql,con=conn)

    cursor.close()
    conn.close()
    return df