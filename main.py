import pandas as pd
import re

pd.set_option("display.max_columns",None)
pd.set_option("display.max_rows",None)

from tkinter import *
import tkinter.filedialog
root = Tk()
def xz():
     filename = tkinter.filedialog.askopenfilename()
     if filename != '':
          lb.config(text = "您选择的文件是："+filename);
          data_df = pd.read_excel(filename, sheet_name=0, header=0)
          # 开始处理
          # 替换处理部分
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("在线学习日语，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("日语在线学习，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("在线日语课程，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("日语学习，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("学习日语，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("日语培训，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("在线，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("每日，", ""), axis=1)
          data_df["title"] = data_df.apply(lambda x: x["title"].replace("在线日语培训，，", ""), axis=1)
          data_df["content"] = data_df.apply(lambda x: x["content"].replace("\t", ""), axis=1)
          data_df["content"] = data_df.apply(lambda x: re.sub("\n+","\n",x["content"]), axis=1)
          # 过滤行
          data_df = data_df[data_df["title"].str.contains("通知")==False]
          data_df = data_df[data_df["title"].str.contains("核心提示")==False]
          data_df = data_df[data_df["title"].str.contains("更新内容")==False]
          data_df = data_df[data_df["title"].str.contains("孚咖日语")==False]
          # 最终输出
          writer = pd.ExcelWriter(filename.split(".")[0]+"-processed."+filename.split(".")[-1])
          data_df.to_excel(writer,engine="xlrd")
          writer.save()
          # for row in data_df.iterrows():
          #     print(row[1]['title'])
          #     print(row[1]['content'])
          #     # print(row[1])
          # 上传数据库

     else:
          lb.config(text = "您没有选择任何文件");


lb = Label(root,text = '')
lb.pack()
btn = Button(root,text="弹出选择文件对话框",command=xz)
btn.pack()
root.mainloop()

